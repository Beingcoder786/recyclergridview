package com.example.recyclergridview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView: androidx.recyclerview.widget.RecyclerView = findViewById(R.id.recycler_view)

        val movieHolder = mutableListOf(
            MovieModel(R.drawable.h1),
            MovieModel(R.drawable.h5),
            MovieModel(R.drawable.h12),
            MovieModel(R.drawable.h3),
            MovieModel(R.drawable.h12),
            MovieModel(R.drawable.h10),
            MovieModel(R.drawable.h15),
            MovieModel(R.drawable.h18),
            MovieModel(R.drawable.h19),
            MovieModel(R.drawable.h4),
            MovieModel(R.drawable.h5),
            MovieModel(R.drawable.h17),
            MovieModel(R.drawable.t1),
            MovieModel(R.drawable.t2)

        )

        val adapter = MovieAdapter(movieHolder) //object create of adaptter
        recyclerView.adapter = adapter//link recycler to adapter
        // recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)


    }
}