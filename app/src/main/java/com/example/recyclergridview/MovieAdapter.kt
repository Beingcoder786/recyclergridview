package com.example.recyclergridview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class MovieAdapter(val movieHolder:List<MovieModel>): RecyclerView.Adapter<MovieWallpaperViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieWallpaperViewHolder {

        val view= LayoutInflater.from(parent.context).inflate(R.layout.movie_view,parent,false)
        return MovieWallpaperViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  movieHolder.size

    }

    override fun onBindViewHolder(holder: MovieWallpaperViewHolder, position: Int) {
        holder.img.setImageResource(movieHolder[position].img)
    }
}