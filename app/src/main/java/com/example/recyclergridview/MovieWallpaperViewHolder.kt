package com.example.recyclergridview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class MovieWallpaperViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val img=itemView.findViewById<ImageView>(R.id.img_view)

}